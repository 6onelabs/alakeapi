﻿namespace AlakeAPI.Models
{
    public class Privileges
    {
        public const string MessageOutBox = "MessageOutBox";
        public const string CanViewDashboard = "CanViewDashboard";
        public const string CanViewReports = "CanViewReports";
        public const string CanViewSettings = "CanViewSettings";
        public const string IsAdministrator = "IsAdministrator";
        public const string CanManageSettings = "CanManageSettings";
        public const string CanSendMessages = "CanSendMessages";
        public const string CanScheduleMessages = "CanScheduleMessages";
        public const string CanViewTemplating = "CanViewTemplating";
        public const string CanManageContacts = "CanManageContacts";
        public const string CanViewLogs = "CanViewLogs";
        public const string CanCancelBatch = "CanCancelBatch";
        public const string CanViewTopups = "CanViewTopups";
        public const string CanManageTopups = "CanManageTopups";
        public const string CanManageEvents = "CanManageEvents";
        public const string CanViewEvents = "CanViewEvents";
        public const string CanManageAttendants = "CanManageAttendants";
        public const string CanViewAttendants = "CanViewAttendants";
    }

    public class GenericProperties
    {
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class DefaultKeys
    {
        public static string CurrencyFormat = "GHC##,###.00";
    }

    public class ConfigKeys
    {
        public static string EmailAccountName = "EmailAccountName";
        public static string EmailApiKey = "EmailApiKey";
        public static string EmailSender = "EmailSender";
        public static string SmsApiKey = "SmsApiKey";
        public static string SmsSender = "SmsSender";
        public static string AppTitle = "AppTitle";
        public static string Logo = "Logo";
        public static string ToolbarColour = "ToolbarColour";
    }

}