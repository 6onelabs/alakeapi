﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AlakeAPI.Models
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext() : base("DefaultConnection") { }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactGroup> ContactGroups { get; set; }
        public DbSet<Sender> Senders { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Templating> Templatings { get; set; }
        public DbSet<TemplatingRecipient> TemplatingRecipients { get; set; }
        public DbSet<NotificationMessage> NotificationMessages { get; set; }
        public DbSet<ResetRequest> ResetRequests { get; set; }
        public DbSet<ScheduleMessage> ScheduleMessages { get; set; }
        public DbSet<ScheduleBirthdayMessage> ScheduleBirthdayMessages { get; set; }
        public DbSet<SentBirthdayMessage> SentBirthdayMessages { get; set; }
        public DbSet<ScheduleRetirementMessage> ScheduleRetirementMessages { get; set; }
        public DbSet<SentRetirementMessage> SentRetirementMessages { get; set; }
        public DbSet<SentWorkAnniversaryMessage> SentWorkAnniversaryMessages { get; set; }
        public DbSet<TopUp> TopUps { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventAttendant> EventAttendants { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, Migrations.Configuration>());
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.CreatedAt = DateTime.UtcNow;
                entry.ModifiedAt = DateTime.UtcNow;
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.ModifiedAt = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }
    }
}