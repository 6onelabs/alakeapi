﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using AlakeAPI.AxHelpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AlakeAPI.Models
{
    public interface IHasId
    {
        [Key]
        long Id { get; set; }
    }

    public interface ISecured
    {
        bool Locked { get; set; }
        bool Hidden { get; set; }
    }

    public interface IAuditable : IHasId
    {
        [Required]
        string CreatedBy { get; set; }
        [Required]
        string ModifiedBy { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime ModifiedAt { get; set; }
    }

    public class HasId : IHasId
    {
        public long Id { get; set; }
    }

    public class AuditFields : HasId, IAuditable, ISecured
    {
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string ModifiedBy { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime ModifiedAt { get; set; } = DateTime.Now;
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
    }

    public class LookUp : AuditFields
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
    }

    public class User : IdentityUser
    {
        [MaxLength(128), Required]
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [Required, MaxLength(128)]
        public override string PhoneNumber { get; set; }
        [Required, MaxLength(128)]
        public override string Email { get; set; }
        public virtual Profile Profile { get; set; }
        public long ProfileId { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public bool IsActive { get; set; } = true;
        public bool IsDeleted { get; set; } = false;
        public bool Locked { get; set; } = false;
        public bool Hidden { get; set; } = false;
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }
    }

    public class Profile : AuditFields
    {
        [Required, MaxLength(512), Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        [MaxLength(500000)]
        public string Privileges { get; set; }
        public List<User> Users { get; set; }
    }

    public class Contact : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(16);
        [Required, MaxLength(128)]
        public string Name { get; set; }
        [MaxLength(32)]
        public string PhoneNumber { get; set; }
        [MaxLength(64)]
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? EmploymentDate { get; set; }
        public virtual List<ContactGroup> Groups { get; set; }
        [NotMapped]
        public string GroupNames { get; set; }
        [NotMapped]
        public List<long> GroupIds { get; set; }
        public ContactStatus Status { get; set; } = ContactStatus.Active;
        public DateTime? LastDobSentDate { get; set; }

    }

    public class ContactGroup
    {
        public long Id { get; set; }
        public long ContactId { get; set; }
        public Contact Contact { get; set; }
        public long GroupId { get; set; }
        public virtual Group Group { get; set; }
    }

    public class Group : LookUp
    {
        public bool IsDeleted { get; set; } = false;
    }

    public class Sender : LookUp
    {
        public bool IsDeleted { get; set; }
    }

    public class Template : LookUp
    {
        public bool IsDeleted { get; set; }
    }

    public class Message : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(32);
        [MaxLength(160)]
        public string BatchName { get; set; }
        [Required, MaxLength(160)]
        public string Sender { get; set; }
        [Required]
        public string Content { get; set; }
        public DateTime SendOn { get; set; }
        public MessageStatus Status { get; set; }
        public MessageType Type { get; set; }
        [Required, MaxLength(160)]
        public string Receipient { get; set; }
        public string Response { get; set; }
        [MaxLength(64)]
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public string FileName { get; set; } = "attachment";
        public string FileBase64String { get; set; }
        public string FileExtension { get; set; } = "pdf"; //pdf/docx/jpeg/png/jpg
    }

    public class ScheduleMessage : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(32);
        [Required, MaxLength(160)]
        public string Sender { get; set; }
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }
        public DateTime SendOn { get; set; }
        public ScheduleMessageStatus Status { get; set; }
        public ScheduleMessageType Type { get; set; }
        [NotMapped]
        public List<long> GroupIds { get; set; }
        public string Groups { get; set; }
        public string ContactIds { get; set; }
        public MessageRecipientType RecipientType { get; set; } = MessageRecipientType.AllContacts;
        public MessageType MessageType { get; set; } = MessageType.SMS;
        public long Count { get; set; }
        public DateTime LastSent { get; set; } = DateTime.Now.AddYears(-1);
    }

    public class ScheduleBirthdayMessage : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(32);
        [Required, MaxLength(160)]
        public string Sender { get; set; }
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }
        public ScheduleMessageStatus Status { get; set; }
        [NotMapped]
        public List<long> GroupIds { get; set; }
        public string Groups { get; set; }
        public string ContactIds { get; set; }
        public MessageRecipientType RecipientType { get; set; } = MessageRecipientType.AllContacts;
        public MessageType MessageType { get; set; } = MessageType.SMS;
        public long Count { get; set; }
        public DateTime LastSent { get; set; } = DateTime.Now.AddYears(-1);
    }

    public class SentBirthdayMessage : HasId
    {
        public DateTime Date { get; set; }
        public long ContactId { get; set; }
        public virtual Contact Contact { get; set; }
        public long ScheduleBirthdayMessageId { get; set; }
        public ScheduleBirthdayMessage ScheduleBirthdayMessage { get; set; }
    }

    public class ScheduleRetirementMessage : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(32);
        [Required, MaxLength(160)]
        public string Sender { get; set; }
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }
        public ScheduleMessageStatus Status { get; set; }
        [NotMapped]
        public List<long> GroupIds { get; set; }
        public string Groups { get; set; }
        public string ContactIds { get; set; }
        public MessageRecipientType RecipientType { get; set; } = MessageRecipientType.AllContacts;
        public MessageType MessageType { get; set; } = MessageType.SMS;
        public long Count { get; set; }
        public DateTime LastSent { get; set; } = DateTime.Now.AddYears(-1);
    }

    public class SentRetirementMessage : HasId
    {
        public DateTime Date { get; set; }
        public long ContactId { get; set; }
        public virtual Contact Contact { get; set; }
        public long ScheduleRetirementMessageId { get; set; }
        public ScheduleRetirementMessage ScheduleRetirementMessage { get; set; }
        public bool IsPreNotice { get; set; } = true;
    }
    public class SentWorkAnniversaryMessage : HasId
    {
        public DateTime Date { get; set; }
        public long ContactId { get; set; }
        public virtual Contact Contact { get; set; }
        public long ScheduleRetirementMessageId { get; set; }
        public ScheduleRetirementMessage ScheduleRetirementMessage { get; set; }
    }

    public class SimpleMessage
    {
        public string Sender { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public MessageType Type { get; set; }
        public string Recipients { get; set; }
    }

    public class BulkMessage
    {
        public string Sender { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime SendAt { get; set; }
        public string SendTime { get; set; }
        public MessageType Type { get; set; }
        public List<Contact> Contacts { get; set; }
    }

    public enum MessageStatus
    {
        Failed,
        Pending,
        Sent,
        Received,
        Deleted,
        Delivered,
        Cancelled
    }

    public enum ScheduleMessageStatus
    {
        Active,
        Inactive
    }
    public enum ScheduleMessageType
    {
        Birthday,
        IndepenceDay,
        MayDay,
        FoundersDay,
        FarmersDay,
        NewYearsDay,
        RetirementNotification
    }
    public enum MessageRecipientType
    {
        AllContacts,
        Groups,
        SelectContacts
    }

    public enum ContactStatus
    {
        Active,
        Blocked
    }

    public class MessageViewModel
    {
        public long Id { get; set; }
        public long Sender { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public long[] Groups { get; set; }
        public List<Contact> Contacts { get; set; }
    }

    public class SmsMessageViewModel
    {
        public long SSender { get; set; }
        public string SMessage { get; set; }
        public string PhoneNumbers { get; set; }
    }
    public class EmailMessageViewModel
    {
        public long ESender { get; set; }
        public string ESubject { get; set; }
        public string EMessage { get; set; }
        public string Emails { get; set; }
    }

    public class SampleMessage
    {
        public string Message { get; set; }
        public string Recipient { get; set; }
        public string Type { get; set; }
    }

    public class NotificationMessage : AuditFields
    {
        [Required, MaxLength(128)]
        public string Subject { get; set; }
        [Required]
        public string Message { get; set; }
        [Required, MaxLength(128)]
        public string Sender { get; set; }
        [Required, MaxLength(128)]
        public string Receiver { get; set; }
        public MessageStatus Status { get; set; }
        public DateTime SendOn { get; set; }
        [Required, MaxLength(128)]
        public string Type { get; set; }
    }

    public class ResetRequest : HasId
    {
        public string Ip { get; set; } = "127.0.0.1";
        public DateTime Date { get; set; } = DateTime.Now;
        public string Email { get; set; }
        public string Token { get; set; }
        public bool IsActive { get; set; } = false;
    }

    public class ResetModel
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }



    public enum MessageType
    {
        SMS,
        Email
    }

    public class Templating : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomString(32);
        [MaxLength(160)]
        public string BatchName { get; set; } = StringHelpers.GenerateRandomString(10).ToUpper();
        [Required, MaxLength(160)]
        public string Sender { get; set; }
        [Required]
        public string Message { get; set; }
        public string Title { get; set; }
        public DateTime SendAt { get; set; }
        public string SendTime { get; set; }
        public TemplatingStatus Status { get; set; }
        public MessageType Type { get; set; }
        public RecipientType RecipientType { get; set; }
        public string Groups { get; set; }
        public virtual List<TemplatingRecipient> Recipients { get; set; }
        [NotMapped]
        public List<long> GroupIds { get; set; }
    }

    public class TemplatingRecipient : HasId
    {
        public virtual Templating Templating { get; set; }
        public long TemplatingId { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
    }

    public class TopUp : AuditFields
    {
        public string Reference { get; set; } = StringHelpers.GenerateRandomNumber(12);
        public double Amount { get; set; }
        public double Tax { get; set; }
        public string PhoneNumber { get; set; }
        public string Network { get; set; }
        public string Token { get; set; }
        public TopupStatus Status { get; set; } = TopupStatus.Pending;
        public string Response { get; set; }
        public string CallbackUrl { get; set; }
    }

    public class Event : AuditFields
    {
        [MaxLength(32), Required]
        public string Code { get; set; } = StringHelpers.GenerateRandomString(16);
        [MaxLength(256), Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Location { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public virtual List<EventAttendant> Attendants { get; set; }
    }

    public class EventAttendant : AuditFields
    {
        public string Code { get; set; } = StringHelpers.GenerateRandomNumber(8);
        public long EventId { get; set; }
        public Event Event { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public bool MessageSent { get; set; }
        public bool TagPrinted { get; set; }
    }

    public enum RecipientType
    {
        Groups,
        Upload
    }
    public enum TemplatingStatus
    {
        Pending,
        Cancelled,
        Processed
    }

    public enum TopupStatus
    {
        Pending,
        Processing,
        Processed,
        Completed,
        Failed,
        Cancelled
    }

}