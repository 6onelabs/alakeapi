﻿using System;
using Microsoft.Owin;
using Owin;
using AlakeAPI;
using System.IO;
using Newtonsoft.Json;

[assembly: OwinStartup(typeof(Startup))]

namespace AlakeAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            LoadSettings();
            ConfigureAuth(app);
        }

        private static void LoadSettings()
        {
            SetupConfig.Setting = JsonConvert.DeserializeObject<Setting>
                (File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SetupConfig.json")));
        }
    }
}
