﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;

namespace AlakeAPI.Services
{
    public class ServicesScheduler
    {
        public static void Start()
        {
            var scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            var topupsService = JobBuilder.Create<TopupsProcessService>().Build();
            var messageService = JobBuilder.Create<MessageProcessService>().Build();
            var scheduleService = JobBuilder.Create<ScheduleProcessService>().Build();
            var msgTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(5)
                        .RepeatForever())
                    .Build();
            var schTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(5)
                        .RepeatForever())
                    .Build();
            var topupsTrigger = TriggerBuilder.Create()
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(5)
                        .RepeatForever())
                    .Build();

            scheduler.ScheduleJob(messageService, msgTrigger);
            scheduler.ScheduleJob(topupsService, topupsTrigger);
            scheduler.ScheduleJob(scheduleService, schTrigger);
        }
    }
}