﻿using AlakeAPI.Models;
using Newtonsoft.Json;
using Quartz;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Text;

namespace AlakeAPI.Services
{
    public class TopupsProcessor
    {
        public void ProcessCreditTopups()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string authorization = "joe5cef9f3c2d101:NWNiNzMwMjNmNTE0NDk0NjRhNzhlNTcxNjY4NTIyMzg=";
            var conversion = Encoding.UTF8.GetBytes(authorization);
            var encoded_authorization = Convert.ToBase64String(conversion);
            //var walletAccount = "005591070722";
            var merchantId = "TTM-00000457";
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var topups = db.TopUps.Where(x => x.Status == TopupStatus.Pending).Take(100).ToList();
                    if (!topups.Any()) return;

                    foreach (var t in topups)
                    {
                        var topup = db.TopUps.First(x => x.Id == t.Id);
                        var number = topup.PhoneNumber;
                        number = number.Replace("-", "");
                        number = number.Replace(" ", "");
                        //strip the country code form it
                        if (!number.StartsWith("233") && !number.StartsWith("0"))
                        {
                            number = "233" + number;
                        }
                        if (number.StartsWith("00233"))
                        {
                            number = "233" + number.Substring(5);
                        }
                        if (number.StartsWith("+233")) number = number.Replace("+233", "233");
                        if (number.Length <= 10)
                        {
                            if (number.StartsWith("02"))
                            {
                                number = "2332" + number.Substring(2);
                            }
                            else if (number.StartsWith("05"))
                            {
                                number = "2335" + number.Substring(2);
                            }
                        }

                        var reqObj = new TheTellerPaymentRequest
                        {
                            processing_code = "000200",
                            merchant_id = merchantId,
                            amount = ((topup.Amount+topup.Tax)*100).ToString("000000000000"),
                            rswitch = GetSwitch(topup.Network),
                            transaction_id = topup.Reference,
                            subscriber_number = number,
                            voucher_code = topup.Token
                        };
                        var obj = JsonConvert.SerializeObject(reqObj);

                        var client = new RestClient("https://prod.theteller.net/v1.1/transaction/process");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Cache-Control", "no-cache");
                        request.AddHeader("Authorization", "Basic " + encoded_authorization);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddParameter("application/json", obj, ParameterType.RequestBody);

                        var res = client.Execute(request);

                        if (res.StatusCode == HttpStatusCode.OK)
                        {
                            var response = JsonConvert.DeserializeObject<TheTellerPaymentResponse>(res.Content);
                            if(response.status.ToLower() == "approved")
                            {
                                topup.Status = TopupStatus.Processed;
                            }
                            else
                            {
                                topup.Status = TopupStatus.Failed;
                            }
                            topup.Response = topup.Response + " \n"+response.code+"::" + response.reason;

                        }
                        topup.ModifiedAt = DateTime.Now;
                        db.SaveChanges();
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        public string GetSwitch(string network)
        {
            switch (network.ToLower())
            {
                case "mtn":
                    return "MTN";
                case "vodafone":
                    return "VDF";
                case "airtel":
                    return "ATL";
                default:
                    return "TGO";
            }
        }
    }
    [DisallowConcurrentExecution]
    public class TopupsProcessService : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new TopupsProcessor().ProcessCreditTopups();
        }
    }

    public class TheTellerPaymentRequest
    {
        public string merchant_id { get; set; }
        public string amount { get; set; }
        public string processing_code { get; set; }
        public string transaction_id { get; set; }
        public string desc { get; set; } = "Credit Topup Test from COCOBOD-HR";
        public string subscriber_number { get; set; }
        public string voucher_code { get; set; }
        [JsonProperty("r-switch")]
        public string rswitch { get; set; }
    }

    public class TheTellerPaymentResponse
    {
        public string status { get; set; }
        public string code { get; set; }
        public string transaction_id { get; set; }
        public string reason { get; set; }
    }
}