﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using AlakeAPI.AxHelpers;
using AlakeAPI.Models;

namespace AlakeAPI.Services
{
    public class MessageProcessor
    {
        public void ProcessTemplatingMessage()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var pending = db.Templatings.Where(x => x.SendAt <= now && x.Status == TemplatingStatus.Pending).OrderBy(x=> x.CreatedAt).Take(100).ToList();

                    foreach (var t in pending)
                    {
                        var tmp = db.Templatings.First(x => x.Id == t.Id);
                        var batch = tmp.Type.ToString()+"-" + StringHelpers.GenerateRandomNumber(8);
                        if(tmp.RecipientType == RecipientType.Groups)
                        {
                            if (tmp.Groups == null) continue;
                            var grpIds = tmp.Groups?.Split(',').ToList();
                            var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                            var conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                            foreach(var c in conts){
                                var message = MessageHelpers.FixContactMessage(tmp.Message, c);
                                var recp = tmp.Type == MessageType.SMS ? c.PhoneNumber : c.Email;
                                if (string.IsNullOrEmpty(recp)) continue;
                                var msg = new Message
                                {
                                    Type = tmp.Type,
                                    BatchName = batch,
                                    Sender = tmp.Sender,
                                    Content = message,
                                    SendOn = tmp.SendAt,
                                    Status = MessageStatus.Pending,
                                    Receipient = recp,
                                    Subject = tmp.Type.ToString() + " - "+ (message.Length<= 20 ? message : message.Substring(0,20)),
                                    CreatedAt = DateTime.Now,
                                    CreatedBy = tmp.CreatedBy,
                                    ModifiedAt = DateTime.Now,
                                    ModifiedBy = tmp.ModifiedBy,
                                };
                                db.Messages.Add(msg);
                            }
                        }
                        else
                        {
                            var conts = db.TemplatingRecipients.Where(x => x.TemplatingId == tmp.Id);
                            foreach (var c in conts)
                            {
                                var message = MessageHelpers.FixTempRecpMessage(tmp.Message, c);
                                var recp = tmp.Type == MessageType.SMS ? c.PhoneNumber : c.Email;
                                var msg = new Message
                                {
                                    Type = tmp.Type,
                                    BatchName = batch,
                                    Sender = tmp.Sender,
                                    Content = message,
                                    SendOn = tmp.SendAt,
                                    Status = MessageStatus.Pending,
                                    Receipient = recp,
                                    Subject = tmp.Type.ToString() + " - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                                    CreatedAt = DateTime.Now,
                                    CreatedBy = tmp.CreatedBy,
                                    ModifiedAt = DateTime.Now,
                                    ModifiedBy = tmp.ModifiedBy,
                                };
                                db.Messages.Add(msg);
                            }
                        }
                        tmp.Status = TemplatingStatus.Processed;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) {
                var ex = e;
            }
        }
        public void SendSms()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var unsentMsgs = db.Messages.Where(x => x.Type == MessageType.SMS && x.Status == MessageStatus.Pending).ToList();

                    foreach (var msg in unsentMsgs)
                    {
                        MessageHelpers.ProcessSms(msg.Id);
                    }
                }
            }
            catch (Exception) { }
        }

        public void SendEmail()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var unsentMsgs = db.Messages.Where(x => x.Type == MessageType.Email && (x.Status == MessageStatus.Pending || x.Status == MessageStatus.Failed)).ToList();

                    foreach (var msg in unsentMsgs)
                    {
                        MessageHelpers.ProcessEmail(msg.Id);
                    }
                }
            }
            catch (Exception) { }
        }

        
    }
    [DisallowConcurrentExecution]
    public class MessageProcessService : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new MessageProcessor().ProcessTemplatingMessage();
            new MessageProcessor().SendSms();
            new MessageProcessor().SendEmail();
        }
    }
    }