﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlakeAPI.AxHelpers;
using AlakeAPI.Models;
using Quartz;

namespace AlakeAPI.Services
{
    public class ScheduleProcessor
    {
        public void ProcessBirthdayMessages()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var bday = db.ScheduleBirthdayMessages.Where(x => x.Status == ScheduleMessageStatus.Active).FirstOrDefault();
                    if (bday == null) return;
                    var batch = "BIRTHDAYMESSAGE-" + StringHelpers.GenerateRandomNumber(8);
                    var conts = new List<Contact>();
                    if (bday.RecipientType == MessageRecipientType.Groups)
                    {
                        if (bday.Groups == null) return;
                        var grpIds = bday.Groups?.Split(',').ToList();
                        var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                        conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                    }
                    else if (bday.RecipientType == MessageRecipientType.AllContacts)
                    {
                        conts = db.Contacts.Where(x => x.Status == ContactStatus.Active).ToList();
                    }

                    foreach (var c in conts)
                    {
                        var contact = db.Contacts.First(x => x.Id == c.Id);
                        if (!contact.DateOfBirth.HasValue) continue;
                        var bd = new DateTime(now.Year, contact.DateOfBirth.Value.Month, contact.DateOfBirth.Value.Day);
                        if (bd < now) contact.LastDobSentDate = bd;
                        if (bd > now) continue;
                        if (contact.LastDobSentDate.HasValue && contact.LastDobSentDate.Value.Year == now.Year) continue;

                        var sent = db.SentBirthdayMessages.Where(x => x.Date.Year == now.Year && x.ContactId == c.Id).FirstOrDefault();
                        if (sent != null) continue;

                        var message = MessageHelpers.FixContactMessage(bday.Message, c);
                        var recp = bday.MessageType == MessageType.SMS ? c.PhoneNumber : c.Email;
                        if (string.IsNullOrEmpty(recp)) continue;
                        var msg = new Message
                        {
                            Type = bday.MessageType,
                            BatchName = batch,
                            Sender = bday.Sender,
                            Content = message,
                            SendOn = now,
                            Status = MessageStatus.Pending,
                            Receipient = recp,
                            Subject = "BIRTHDAYMESSAGE - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                            CreatedAt = DateTime.Now,
                            CreatedBy = bday.ModifiedBy,
                            ModifiedAt = DateTime.Now,
                            ModifiedBy = bday.ModifiedBy,
                        };
                        db.Messages.Add(msg);
                        contact.LastDobSentDate = now;
                        bday.Count = bday.Count + 1;
                        var alreadysent = new SentBirthdayMessage
                        {
                            Date = now,
                            ContactId = c.Id,
                            ScheduleBirthdayMessageId = bday.Id
                        };
                        db.SentBirthdayMessages.Add(alreadysent);
                    }
                    bday.LastSent = now;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        public void ProcessHolidayMessages()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var msgs = db.ScheduleMessages.Where(x => x.Status == ScheduleMessageStatus.Active && x.SendOn.Month == now.Month && x.SendOn.Day == now.Day).ToList();

                    foreach (var mg in msgs)
                    {
                        if (mg.LastSent.Year == now.Year) continue;
                        var conts = new List<Contact>();
                        var batch = mg.Type.ToString().ToUpper() + "-" + StringHelpers.GenerateRandomNumber(8);
                        if (mg.RecipientType == MessageRecipientType.Groups)
                        {
                            if (mg.Groups == null) return;
                            var grpIds = mg.Groups?.Split(',').ToList();
                            var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                            conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                        }
                        else if (mg.RecipientType == MessageRecipientType.AllContacts)
                        {
                            conts = db.Contacts.Where(x => x.Status == ContactStatus.Active).ToList();
                        }

                        foreach (var c in conts)
                        {
                            var message = MessageHelpers.FixContactMessage(mg.Message, c);
                            var recp = mg.MessageType == MessageType.SMS ? c.PhoneNumber : c.Email;
                            if (string.IsNullOrEmpty(recp)) continue;
                            var msg = new Message
                            {
                                Type = mg.MessageType,
                                BatchName = batch,
                                Sender = mg.Sender,
                                Content = message,
                                SendOn = now,
                                Status = MessageStatus.Pending,
                                Receipient = recp,
                                Subject = mg.Type.ToString() + " - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                                CreatedAt = DateTime.Now,
                                CreatedBy = mg.ModifiedBy,
                                ModifiedAt = DateTime.Now,
                                ModifiedBy = mg.ModifiedBy,
                            };
                            db.Messages.Add(msg);
                            mg.Count = mg.Count + 1;
                        }
                        mg.LastSent = now;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        public void ProcessPreRetirementMessages()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var retMsg = db.ScheduleRetirementMessages.Where(x => x.Status == ScheduleMessageStatus.Active && x.Title == "Retirement 6 Months Notice").FirstOrDefault();
                    if (retMsg == null) return;
                    var batch = "PRERETIREMENT-" + StringHelpers.GenerateRandomNumber(8);
                    var conts = new List<Contact>();
                    if (retMsg.RecipientType == MessageRecipientType.Groups)
                    {
                        if (retMsg.Groups == null) return;
                        var grpIds = retMsg.Groups?.Split(',').ToList();
                        var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                        conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                    }
                    else if (retMsg.RecipientType == MessageRecipientType.AllContacts)
                    {
                        conts = db.Contacts.Where(x => x.Status == ContactStatus.Active).ToList();
                    }

                    foreach (var c in conts)
                    {
                        var contact = db.Contacts.First(x => x.Id == c.Id);
                        if (!contact.DateOfBirth.HasValue) continue;

                        var sent = db.SentRetirementMessages.Where(x => x.Date.Year == now.Year && x.ContactId == c.Id && x.IsPreNotice).FirstOrDefault();
                        if (sent != null) continue;

                        var age = DateHelpers.CalculateAge(contact.DateOfBirth.Value.AddMonths(6));
                        if (age != 60) continue;

                        var message = MessageHelpers.FixContactMessage(retMsg.Message, c);
                        var recp = retMsg.MessageType == MessageType.SMS ? c.PhoneNumber : c.Email;
                        if (string.IsNullOrEmpty(recp)) continue;
                        var msg = new Message
                        {
                            Type = retMsg.MessageType,
                            BatchName = batch,
                            Sender = retMsg.Sender,
                            Content = message,
                            SendOn = now,
                            Status = MessageStatus.Pending,
                            Receipient = recp,
                            Subject = "PRE-RETIREMENT NOTICE - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                            CreatedAt = DateTime.Now,
                            CreatedBy = retMsg.ModifiedBy,
                            ModifiedAt = DateTime.Now,
                            ModifiedBy = retMsg.ModifiedBy,
                        };
                        db.Messages.Add(msg);
                        retMsg.Count = retMsg.Count + 1;
                        var alreadysent = new SentRetirementMessage
                        {
                            Date = now,
                            ContactId = c.Id,
                            ScheduleRetirementMessageId = retMsg.Id,
                            IsPreNotice = true
                        };
                        db.SentRetirementMessages.Add(alreadysent);
                    }
                    retMsg.LastSent = now;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        public void ProcessRetirementMessages()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var retMsg = db.ScheduleRetirementMessages.Where(x => x.Status == ScheduleMessageStatus.Active && x.Title == "Retirement Notice").FirstOrDefault();
                    if (retMsg == null) return;
                    var batch = "RETIREMENT-" + StringHelpers.GenerateRandomNumber(8);
                    var conts = new List<Contact>();
                    if (retMsg.RecipientType == MessageRecipientType.Groups)
                    {
                        if (retMsg.Groups == null) return;
                        var grpIds = retMsg.Groups?.Split(',').ToList();
                        var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                        conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                    }
                    else if (retMsg.RecipientType == MessageRecipientType.AllContacts)
                    {
                        conts = db.Contacts.Where(x => x.Status == ContactStatus.Active).ToList();
                    }

                    foreach (var c in conts)
                    {
                        var contact = db.Contacts.First(x => x.Id == c.Id);
                        if (!contact.DateOfBirth.HasValue) continue;

                        var sent = db.SentRetirementMessages.Where(x => x.Date.Year == now.Year && x.ContactId == c.Id && !x.IsPreNotice).FirstOrDefault();
                        if (sent != null) continue;

                        var age = DateHelpers.CalculateAge(contact.DateOfBirth.Value);
                        if (age != 60) continue;

                        var message = MessageHelpers.FixContactMessage(retMsg.Message, c);
                        var recp = retMsg.MessageType == MessageType.SMS ? c.PhoneNumber : c.Email;
                        if (string.IsNullOrEmpty(recp)) continue;
                        var msg = new Message
                        {
                            Type = retMsg.MessageType,
                            BatchName = batch,
                            Sender = retMsg.Sender,
                            Content = message,
                            SendOn = now,
                            Status = MessageStatus.Pending,
                            Receipient = recp,
                            Subject = "RETIREMENT NOTICE - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                            CreatedAt = DateTime.Now,
                            CreatedBy = retMsg.ModifiedBy,
                            ModifiedAt = DateTime.Now,
                            ModifiedBy = retMsg.ModifiedBy,
                        };
                        db.Messages.Add(msg);
                        retMsg.Count = retMsg.Count + 1;
                        var alreadysent = new SentRetirementMessage
                        {
                            Date = now,
                            ContactId = c.Id,
                            ScheduleRetirementMessageId = retMsg.Id,
                            IsPreNotice = false
                        };
                        db.SentRetirementMessages.Add(alreadysent);
                    }
                    retMsg.LastSent = now;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        public void ProcessWorkAnniversaryMessages()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var now = DateTime.Now;
                    var anniMsg = db.ScheduleRetirementMessages.Where(x => x.Status == ScheduleMessageStatus.Active && x.Title == "Work Anniversary").FirstOrDefault();
                    if (anniMsg == null) return;
                    var batch = "WORKANNIVERSARY-" + StringHelpers.GenerateRandomNumber(8);
                    var conts = new List<Contact>();
                    if (anniMsg.RecipientType == MessageRecipientType.Groups)
                    {
                        if (anniMsg.Groups == null) return;
                        var grpIds = anniMsg.Groups?.Split(',').ToList();
                        var contIds = db.ContactGroups.Where(x => grpIds.Contains(x.GroupId.ToString())).Select(x => x.ContactId).Distinct().ToList();
                        conts = db.Contacts.Where(x => contIds.Contains(x.Id) && x.Status == ContactStatus.Active).ToList();
                    }
                    else if (anniMsg.RecipientType == MessageRecipientType.AllContacts)
                    {
                        conts = db.Contacts.Where(x => x.Status == ContactStatus.Active).ToList();
                    }

                    foreach (var c in conts)
                    {
                        var contact = db.Contacts.First(x => x.Id == c.Id);
                        if (!contact.DateOfBirth.HasValue) continue;

                        var sent = db.SentWorkAnniversaryMessages.Where(x => x.Date.Year == now.Year && x.ContactId == c.Id).FirstOrDefault();
                        if (sent != null) continue;
                        if (!contact.EmploymentDate.HasValue) continue;
                        var age = DateHelpers.CalculateAge(contact.EmploymentDate.Value);
                        if (age % 5 != 0) continue;

                        var message = MessageHelpers.FixAnniversaryMessage(anniMsg.Message, c);
                        var recp = anniMsg.MessageType == MessageType.SMS ? c.PhoneNumber : c.Email;
                        if (string.IsNullOrEmpty(recp)) continue;
                        var msg = new Message
                        {
                            Type = anniMsg.MessageType,
                            BatchName = batch,
                            Sender = anniMsg.Sender,
                            Content = message,
                            SendOn = now,
                            Status = MessageStatus.Pending,
                            Receipient = recp,
                            Subject = "WORK ANNIVERSARY - " + (message.Length <= 20 ? message : message.Substring(0, 20)),
                            CreatedAt = DateTime.Now,
                            CreatedBy = anniMsg.ModifiedBy,
                            ModifiedAt = DateTime.Now,
                            ModifiedBy = anniMsg.ModifiedBy,
                        };
                        db.Messages.Add(msg);
                        anniMsg.Count = anniMsg.Count + 1;
                        var alreadysent = new SentWorkAnniversaryMessage
                        {
                            Date = now,
                            ContactId = c.Id,
                            ScheduleRetirementMessageId = anniMsg.Id
                        };
                        db.SentWorkAnniversaryMessages.Add(alreadysent);
                    }
                    anniMsg.LastSent = now;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }

        
    }

    [DisallowConcurrentExecution]
    public class ScheduleProcessService : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new ScheduleProcessor().ProcessBirthdayMessages();
            new ScheduleProcessor().ProcessPreRetirementMessages();
            new ScheduleProcessor().ProcessRetirementMessages();
            new ScheduleProcessor().ProcessHolidayMessages();
        }
    }
}