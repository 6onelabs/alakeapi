namespace AlakeAPI.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Models.AppDbContext>
    {
        public UserManager<User> UserManager { get; private set; }

        public Configuration()
            : this(new UserManager<User>(new UserStore<User>(new AppDbContext())))
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        public Configuration(UserManager<User> userManager) { UserManager = userManager; }

        protected override void Seed(AppDbContext context)
        {
            #region Roles [Privileges]
            var roles = new List<IdentityRole>
                        {
                            new IdentityRole {Name = Privileges.CanViewDashboard},
                            new IdentityRole {Name = Privileges.CanViewReports},
                            new IdentityRole {Name = Privileges.CanViewSettings},
                            new IdentityRole {Name = Privileges.IsAdministrator},
                            new IdentityRole {Name = Privileges.CanManageSettings},
                            new IdentityRole {Name = Privileges.CanSendMessages},
                            new IdentityRole {Name = Privileges.CanScheduleMessages},
                            new IdentityRole {Name = Privileges.CanViewTemplating},
                            new IdentityRole {Name = Privileges.CanManageContacts},
                            new IdentityRole {Name = Privileges.CanViewLogs},
                            new IdentityRole {Name = Privileges.CanCancelBatch},
                            new IdentityRole {Name = Privileges.CanViewTopups},
                            new IdentityRole {Name = Privileges.CanManageTopups},
                            new IdentityRole {Name = Privileges.CanManageEvents},
                            new IdentityRole {Name = Privileges.CanManageAttendants},
                            new IdentityRole {Name = Privileges.CanViewEvents},
                            new IdentityRole {Name = Privileges.CanViewAttendants}
                        };

            roles.ForEach(r => context.Roles.AddOrUpdate(q => q.Name, r));
            var a = "";
            roles.ForEach(q => a += q.Name + ",");
            #endregion

            #region App Roles
            var adminProfile = new Profile
            {
                Name = "Administrator",
                Notes = "Administrator Role",
                Privileges = a.Trim(','),
                Locked = true,
                CreatedBy = "System",
                ModifiedBy = "System"
            };
            #endregion

            #region Users
            var userManager = new UserManager<User>(new UserStore<User>(context))
            {
                UserValidator = new UserValidator<User>(UserManager)
                {
                    AllowOnlyAlphanumericUserNames = false
                }
            };

            //Admin User
            if (UserManager.FindByNameAsync("admin").Result == null)
            {
                var res = userManager.CreateAsync(new User
                {
                    FullName = "Administrator",
                    PhoneNumber = "0246903913",
                    Email = "biggash730@gmail.com",
                    Profile = adminProfile,
                    UserName = "admin",
                    DateOfBirth = DateTime.Now.AddYears(30),
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    Locked = true,
                }, "p@ssword");

                if (res.Result.Succeeded)
                {
                    var userId = userManager.FindByNameAsync("admin").Result.Id;
                    roles.ForEach(q => userManager.AddToRole(userId, q.Name));
                }
            }

            #endregion

            #region Update Roles
            roles.ForEach(q => context.Roles.AddOrUpdate(q));
            #endregion

            #region Schedules
            var now = DateTime.Now;
            var bday = new ScheduleBirthdayMessage
            {
                Sender = "System",
                Title = "Happy Birthday",
                Message = "Happy Birthday to you [NAME]",
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exBday = context.ScheduleBirthdayMessages.FirstOrDefault(x => x.Title == bday.Title);
            if(exBday == null)
            {
                context.ScheduleBirthdayMessages.AddOrUpdate(x => x.Title, bday);
                context.SaveChanges();
            }
            

            var indeday = new ScheduleMessage
            {
                Type = ScheduleMessageType.IndepenceDay,
                Sender = "System",
                Title = "Happy Independence Day",
                Message = "Happy Independence Day to you [NAME]",
                SendOn = new DateTime(now.Year,3,6,00,10,00),
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exInde = context.ScheduleMessages.FirstOrDefault(x => x.Type == indeday.Type);
            if (exInde == null)
            {
                context.ScheduleMessages.AddOrUpdate(x => x.Type, indeday);
                context.SaveChanges();
            }

            var mayday = new ScheduleMessage
            {
                Type = ScheduleMessageType.MayDay,
                Sender = "System",
                Title = "Happy May Day",
                Message = "Happy May Day to you [NAME]",
                SendOn = new DateTime(now.Year, 5, 1, 00, 00, 00),
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exmayday = context.ScheduleMessages.FirstOrDefault(x => x.Type == mayday.Type);
            if (exmayday == null)
            {
                context.ScheduleMessages.AddOrUpdate(x => x.Type, mayday);
                context.SaveChanges();
            }

            var foundersday = new ScheduleMessage
            {
                Type = ScheduleMessageType.FoundersDay,
                Sender = "System",
                Title = "Happy Founder's Day",
                Message = "Happy Founder's Day to you [NAME]",
                SendOn = new DateTime(now.Year, 8, 4, 00, 00, 00),
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            context.ScheduleMessages.AddOrUpdate(x=> x.Type, foundersday);
            context.SaveChanges();

            var farmersday = new ScheduleMessage
            {
                Type = ScheduleMessageType.FarmersDay,
                Sender = "System",
                Title = "Happy Farmer's Day",
                Message = "Happy Farmer's Day to you [NAME]",
                SendOn = new DateTime(now.Year, 12, 6, 00, 00, 00),
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exfarmersday = context.ScheduleMessages.FirstOrDefault(x => x.Type == farmersday.Type);
            if (exfarmersday == null)
            {
                context.ScheduleMessages.AddOrUpdate(x => x.Type, farmersday);
                context.SaveChanges();
            }

            var newYearDay = new ScheduleMessage
            {
                Type = ScheduleMessageType.NewYearsDay,
                Sender = "System",
                Title = "Happy New Year",
                Message = "Happy New Year to you [NAME]",
                SendOn = new DateTime(now.Year, 1, 1, 00, 00, 00),
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exnewYearDay = context.ScheduleMessages.FirstOrDefault(x => x.Type == newYearDay.Type);
            if (exnewYearDay == null)
            {
                context.ScheduleMessages.AddOrUpdate(x => x.Type, newYearDay);
                context.SaveChanges();
            }

            var retire1 = new ScheduleRetirementMessage
            {
                Sender = "System",
                Title = "Retirement 6 Months Notice",
                Message = "Hello [NAME], We are pleased to remind you that you will be due for retirement in 6 months. Congratulations.",
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exRetire1 = context.ScheduleRetirementMessages.FirstOrDefault(x => x.Title == retire1.Title);
            if (exRetire1 == null)
            {
                context.ScheduleRetirementMessages.AddOrUpdate(x => x.Title, retire1);
                context.SaveChanges();
            }

            var retire = new ScheduleRetirementMessage
            {
                Sender = "System",
                Title = "Retirement Notice",
                Message = "Hello [NAME], We are pleased to remind you that you are due for retirement. Congratulations.",
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var exRetire = context.ScheduleRetirementMessages.FirstOrDefault(x => x.Title == retire.Title);
            if (exRetire == null)
            {
                context.ScheduleRetirementMessages.AddOrUpdate(x => x.Title, retire);
                context.SaveChanges();
            }

            var anniversary = new ScheduleRetirementMessage
            {
                Sender = "System",
                Title = "Work Anniversary",
                Message = "PAMPANAAAAAAA... Congratulations [NAME] on your work anniversary. You are [WORKAGE] years with us today. Enjoy!!!",
                Status = ScheduleMessageStatus.Inactive,
                CreatedBy = "System",
                ModifiedBy = "System",
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now
            };
            var extanniversary = context.ScheduleRetirementMessages.FirstOrDefault(x => x.Title == anniversary.Title);
            if (extanniversary == null)
            {
                context.ScheduleRetirementMessages.AddOrUpdate(x => x.Title, anniversary);
                context.SaveChanges();
            }

            #endregion



            context.SaveChanges();
            base.Seed(context);
        }
    }
}
