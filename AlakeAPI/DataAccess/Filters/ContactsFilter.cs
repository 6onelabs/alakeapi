﻿using System;
using System.Data.Entity;
using System.Linq;
using AlakeAPI.Models;

namespace AlakeAPI.DataAccess.Filters
{
    public class ContactsFilter : Filter<Contact>
    {
        public long Id;
        public string Name;
        public string PhoneNumber;
        public string Email;
        public long GroupId;
        public DateTime? DateOfBirth;

        public override IQueryable<Contact> BuildQuery(IQueryable<Contact> query)
        {
            query = query.Include(x => x.Groups);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (DateOfBirth.HasValue)
            {
                query = query.Where(q => q.DateOfBirth.Value.Year == DateOfBirth.Value.Year && q.DateOfBirth.Value.Month == DateOfBirth.Value.Month && q.DateOfBirth.Value.Day == DateOfBirth.Value.Day);
            }
            return query;
        }
    }

    public class TopupsFilter : Filter<TopUp>
    {
        public long Id;
        public string PhoneNumber;
        public string Network;
        public TopupStatus? Status;

        public override IQueryable<TopUp> BuildQuery(IQueryable<TopUp> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(q => q.PhoneNumber.ToLower().Contains(PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Network)) query = query.Where(q => q.Network.ToLower().Contains(Network.ToLower()));
            if (Status.HasValue)
            {
                query = query.Where(q => q.Status == Status);
            }
            return query;
        }
    }

    public class TemplatingsFilter : Filter<Templating>
    {
        public long Id;
        public string Title;
        public string Message;

        public override IQueryable<Templating> BuildQuery(IQueryable<Templating> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrEmpty(Title)) query = query.Where(q => q.Title.ToLower().Contains(Title.ToLower()));
            if (!string.IsNullOrEmpty(Message)) query = query.Where(q => q.Message.ToLower().Contains(Message.ToLower()));
            return query;
        }
    }

}