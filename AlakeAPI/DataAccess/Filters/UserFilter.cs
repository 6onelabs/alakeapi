using System;
using System.Linq;
using AlakeAPI.Models;

namespace AlakeAPI.DataAccess.Filters
{
    public class UserFilter : Filter<User>
    {
        public long ProfileId;
        public string UserName;
        public string FullName;
        public string Email;
        public long RoleId;
        public DateTime? DateFrom;
        public DateTime? DateTo;

        public override IQueryable<User> BuildQuery(IQueryable<User> query)
        {

            if (ProfileId > 0) query = query.Where(q => q.Profile.Id == ProfileId);
            if (RoleId > 0) query = query.Where(q => q.Profile.Id == RoleId);
            if (!string.IsNullOrEmpty(UserName)) query = query.Where(q => q.UserName.ToLower().Contains(UserName.ToLower()));
            if (!string.IsNullOrEmpty(FullName)) query = query.Where(q => q.FullName.ToLower().Contains(FullName.ToLower()));
            if (!string.IsNullOrEmpty(Email)) query = query.Where(q => q.Email.ToLower().Contains(Email.ToLower()));
            if (DateFrom.HasValue)
            {
                DateFrom = new DateTime(DateFrom.Value.Year, DateFrom.Value.Month, DateFrom.Value.Day, 0, 0, 0);
                query = query.Where(q => q.CreatedAt >= DateFrom);
            }
            if (DateTo.HasValue)
            {
                DateTo = new DateTime(DateTo.Value.Year, DateTo.Value.Month, DateTo.Value.Day, 23, 59, 59);
                query = query.Where(q => q.CreatedAt <= DateTo);
            }

            return query;
        }
    }

}