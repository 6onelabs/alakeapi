﻿using System.Data.Entity;
using System.Linq;
using AlakeAPI.Models;

namespace AlakeAPI.DataAccess.Filters
{
    public class MessagesFilter : Filter<Message>
    {
        public long Id;
        public MessageStatus? Status;
        public MessageType? Type;
        public string Title;
        public string Message;
        public string Emails;
        public string PhoneNumbers;
        public string Batch;


        public override IQueryable<Message> BuildQuery(IQueryable<Message> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (Status.HasValue) query = query.Where(x => x.Status == Status);
            if (Type.HasValue) query = query.Where(x => x.Type == Type);
            if (!string.IsNullOrEmpty(Title)) query = query.Where(x => x.Subject.ToLower().Contains(Title.ToLower()));
            if (!string.IsNullOrEmpty(Batch)) query = query.Where(x => x.BatchName.ToLower().Contains(Batch.ToLower()));
            if (!string.IsNullOrEmpty(Message)) query = query.Where(x => x.Content.ToLower().Contains(Message.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumbers)) query = query.Where(x => PhoneNumbers.ToLower().Contains(x.Receipient.ToLower()));
            if (!string.IsNullOrEmpty(Emails)) query = query.Where(x => Emails.ToLower().Contains(x.Receipient.ToLower()));

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class EventsFilter : Filter<Event>
    {
        public long Id;
        public string Title;
        public string Code;
        public string Location;
        public bool? IsActive;

        public override IQueryable<Event> BuildQuery(IQueryable<Event> query)
        {
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (!string.IsNullOrEmpty(Title)) query = query.Where(x => x.Title.ToLower().Contains(Title.ToLower()));
            if (!string.IsNullOrEmpty(Code)) query = query.Where(x => x.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(Location)) query = query.Where(x => Location.ToLower().Contains(x.Location.ToLower()));
            if (IsActive.HasValue) query = query.Where(x => x.IsActive == IsActive.HasValue);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class EventAttendantsFilter : Filter<EventAttendant>
    {
        public long Id;
        public long EventId;
        public string EventTitle;
        public string EventCode;
        public string FullName;
        public string PhoneNumber;
        public string Company;
        public string Position;
        public bool? MessageSent;
        public bool? TagPrinted;

        public override IQueryable<EventAttendant> BuildQuery(IQueryable<EventAttendant> query)
        {
            query = query.Include(x => x.Event);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (EventId > 0) query = query.Where(q => q.EventId == EventId);
            if (!string.IsNullOrEmpty(EventTitle)) query = query.Where(x => x.Event.Title.ToLower().Contains(EventTitle.ToLower()));
            if (!string.IsNullOrEmpty(EventCode)) query = query.Where(x => x.Event.Code.ToLower().Contains(EventCode.ToLower()));
            if (!string.IsNullOrEmpty(FullName)) query = query.Where(x => FullName.ToLower().Contains(x.FullName.ToLower()));
            if (!string.IsNullOrEmpty(PhoneNumber)) query = query.Where(x => PhoneNumber.ToLower().Contains(x.PhoneNumber.ToLower()));
            if (!string.IsNullOrEmpty(Company)) query = query.Where(x => Company.ToLower().Contains(x.Company.ToLower()));
            if (!string.IsNullOrEmpty(Position)) query = query.Where(x => Position.ToLower().Contains(x.Position.ToLower()));
            if (MessageSent.HasValue) query = query.Where(x => x.MessageSent == MessageSent.HasValue);
            if (TagPrinted.HasValue) query = query.Where(x => x.TagPrinted == TagPrinted.HasValue);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }
}