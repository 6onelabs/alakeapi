﻿using AlakeAPI.Models;
using System;
using System.Linq;
using WebGrease.Css.Extensions;

namespace AlakeAPI.DataAccess.Repositories
{
    public class EventRepository : BaseRepository<Event>
    {
        public void UpdateEventCode(long eventId)
        {
            var rec = DbContext.Events.First(x => x.Id == eventId);
            var id = $"{rec.Id:D5}";
            rec.Code = $"EVENT/{DateTime.Now.Year}/{id}";
            SaveChanges();
        }

        public void Activate(long eventId)
        {
            var recs = DbSet.Where(x => x.Id != eventId);
            recs.ForEach(x => x.IsActive = false);
            var rec = DbSet.First(x => x.Id == eventId);
            rec.IsActive = true;
            SaveChanges();
        }

        public void Deactivate(long eventId)
        {
            var rec = DbSet.First(x => x.Id == eventId);
            rec.IsActive = true;
            SaveChanges();
        }

        public Event GetActiveEvent()
        {
            return DbContext.Events.FirstOrDefault(x => x.IsActive);
        }

        public Event ActiveEvent(AppDbContext db)
        {
            return db.Events.FirstOrDefault(x => x.IsActive);
        }
    }
}