﻿using System.Collections.Generic;

namespace AlakeAPI
{
    public static class SetupConfig
    {
        public static Setting Setting { get; set; }
    }

    public class Setting
    {
        public List<string> Wildcards { get; set; }
        public SmsMessageService SmsMessageService { get; set; }
    }

    public class SmsMessageService
    {
        public string SenderId { get; set; }
        public string BaseUrl { get; set; }
        public string BalanceUrl { get; set; }
        public string BasicToken { get; set; }
        public string ApiKey { get; set; }
    }

    public class Connections
    {
        public string Name { get; set; }
        public string ApiKey { get; set; }
    }
}