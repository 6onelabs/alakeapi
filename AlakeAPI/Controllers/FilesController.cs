﻿using AlakeAPI.AxHelpers;
using AlakeAPI.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/files")]
    public class FilesController : ApiController
    {
        [HttpGet]
        [Route("downloadcontactsuploadtemplate")]
        public ResultObj DownloadContactsUploadTemplate()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var template = HttpContext.Current
                .Server.MapPath(@"~/Documents/Templates/ContactsUpload.xlsx");
                byte[] bytes = File.ReadAllBytes(template);
                var res = Convert.ToBase64String(bytes);
                results = WebHelpers.BuildResponse(res, "Downloaded Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("downloademplatingsuploadtemplate")]
        public ResultObj DownloadTemplatingUploadTemplate()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var template = HttpContext.Current
                .Server.MapPath(@"~/Documents/Templates/TemplatingUpload.xlsx");
                byte[] bytes = File.ReadAllBytes(template);
                var res = Convert.ToBase64String(bytes);
                results = WebHelpers.BuildResponse(res, "Downloaded Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
