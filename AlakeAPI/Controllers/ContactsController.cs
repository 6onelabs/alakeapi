﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/contacts")]
    public class ContactsController : BaseApi<Contact>
    {
        public override ResultObj Post(Contact record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        if(record.DateOfBirth.HasValue) record.DateOfBirth = record.DateOfBirth.Value.AddHours(1);
                        record.CreatedAt = DateTime.Now;
                        record.ModifiedAt = DateTime.Now;
                        record.CreatedBy = user.UserName;
                        record.ModifiedBy = user.UserName;
                        var groupIds = record.GroupIds;
                        db.Contacts.Add(record);
                        db.SaveChanges();
                        
                        foreach(var id in groupIds)
                        {
                            db.ContactGroups.Add(new ContactGroup
                            {
                                ContactId = record.Id,
                                GroupId = id
                            });
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        public override ResultObj Put(Contact record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var cont = db.Contacts.FirstOrDefault(x => x.Id == record.Id);
                        if (cont == null) throw new Exception("Please check the id");
                        if (record.DateOfBirth.HasValue) record.DateOfBirth = record.DateOfBirth.Value.AddHours(1);

                        cont.ModifiedAt = DateTime.Now;
                        cont.ModifiedBy = user.UserName;
                        cont.Name = record.Name;
                        cont.PhoneNumber = record.PhoneNumber;
                        cont.Email = record.Email;
                        cont.DateOfBirth = record.DateOfBirth;
                        cont.EmploymentDate = record.EmploymentDate;
                        var groupIds = record.GroupIds;
                        db.SaveChanges();
                        var exst = db.ContactGroups.Where(x => x.ContactId == record.Id);
                        db.ContactGroups.RemoveRange(exst);
                        db.SaveChanges();
                        foreach (var id in groupIds)
                        {
                            db.ContactGroups.Add(new ContactGroup
                            {
                                ContactId = record.Id,
                                GroupId = id
                            });
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(ContactsFilter filter)
        {
            ResultObj results;
            try
            {
                var data = Repository.Query(filter).Include(x => x.Groups).OrderBy(x => x.Status).ThenBy(x=> x.Name).ToList();
                var total = data.Count();
                if (filter.Pager.Size > 0) data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                var res = data.Select(x =>
                    new
                    {
                        x.Id,
                        x.Name,
                        x.Reference,
                        x.PhoneNumber,
                        x.Email,
                        x.DateOfBirth,
                        x.EmploymentDate,
                        Status = x.Status.ToString(),
                        GroupNames = string.Join(",", x.Groups.Select(g => g.Group.Name).ToList()),
                        //GroupIds = x.Groups.Select(g => g.GroupId).ToList(),
                        Groups = x.Groups.Select(g=> g.GroupId).Distinct().Count(),
                        x.CreatedAt,
                        x.ModifiedAt,
                        x.CreatedBy,
                        x.ModifiedBy
                    }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().Where(x=> x.Status == ContactStatus.Active).OrderBy(x => x.Name).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.Name,
                          x.Reference,
                          x.PhoneNumber,
                          x.Email,
                          x.DateOfBirth,
                          x.EmploymentDate,
                          Status = x.Status.ToString(),
                          GroupNames = string.Join(",", x.Groups.Select(g => g.Group.Name).ToList()),
                          GroupIds = x.Groups.Select(g => g.GroupId).ToList(),
                          Groups = x.Groups.Select(g => new
                          {
                              g.Group.Id,
                              g.Group.Name,
                              g.Group.Description
                          }).ToList(),
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get(new ContactsFilter { Id = id }).OrderBy(x => x.Name).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.Name,
                          x.Reference,
                          x.PhoneNumber,
                          x.Email,
                          x.DateOfBirth,
                          x.EmploymentDate,
                          Status = x.Status.ToString(),
                          GroupNames = string.Join(",", x.Groups.Select(g => g.Group.Name).ToList()),
                          GroupIds = x.Groups.Select(g => (long)g.GroupId).ToList(),
                          Groups = x.Groups.Select(g => g.GroupId).Distinct().Count(),
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).First();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Contacts.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Could not find the contact");

                var cg = db.ContactGroups.Where(x => x.ContactId == id);
                db.ContactGroups.RemoveRange(cg);

                db.Contacts.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("saveupload")]
        public ResultObj SaveUpload(List<Contact> records)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;

                        foreach(var record in records)
                        {
                            record.CreatedAt = DateTime.Now;
                            record.ModifiedAt = DateTime.Now;
                            record.CreatedBy = user.UserName;
                            record.ModifiedBy = user.UserName;
                            var groupIds = record.GroupIds;
                            db.Contacts.Add(record);
                            db.SaveChanges();

                            foreach (var id in groupIds)
                            {
                                db.ContactGroups.Add(new ContactGroup
                                {
                                    ContactId = record.Id,
                                    GroupId = id
                                });
                            }
                            db.SaveChanges();
                        }                        
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(null, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        [HttpGet]
        [Route("activate")]
        public ResultObj Activate(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Contacts.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Could not find the contact");
                if (rec.Status == ContactStatus.Active) throw new Exception("Contact is already active");
                rec.Status = ContactStatus.Active;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Activated", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("block")]
        public ResultObj Block(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Contacts.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Could not find the contact");
                if (rec.Status == ContactStatus.Blocked) throw new Exception("Contact is already blocked");
                rec.Status = ContactStatus.Blocked;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Blocked", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("groupscontacts")]
        public ResultObj GetGroupContacts(List<long> obj)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ContactGroups.Where(x=> obj.Contains(x.GroupId)).Include(x=> x.Contact).Select(x=> x.Contact).Where(x=> x.Status == ContactStatus.Active).Distinct().OrderBy(x => x.Name).ToList().Select(x =>
                    new
                    {
                        x.Id,
                        x.Name,
                        x.Reference,
                        x.PhoneNumber,
                        x.Email,
                    }).ToList();

                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
