﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/templating")]
    public class TemplatingController : BaseApi<Templating>
    {
        public override ResultObj Post(Templating record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var sendTime = record.SendTime.Split(':').ToList();
                        record.SendAt = record.SendAt.AddHours(1);
                        record.SendAt = new DateTime(record.SendAt.Year, record.SendAt.Month, record.SendAt.Day, int.Parse(sendTime[0]), int.Parse(sendTime[1]),0);

                        var recps = record.Recipients;
                        if(record.RecipientType == RecipientType.Groups)
                        {
                            record.Recipients = null;
                            record.Groups = string.Join(",", record.GroupIds);
                        }
                        else
                        {
                            record.Groups = null;
                            record.Recipients = null;
                        }

                        record.CreatedAt = DateTime.Now;
                        record.ModifiedAt = DateTime.Now;
                        record.CreatedBy = user.UserName;
                        record.ModifiedBy = user.UserName;
                        db.Templatings.Add(record);
                        db.SaveChanges();

                        if(record.RecipientType == RecipientType.Upload)
                        {

                            foreach (var r in recps)
                            {
                                r.TemplatingId = record.Id;
                                db.TemplatingRecipients.Add(r);
                            };
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        public override ResultObj Put(Templating record)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(record, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(TemplatingsFilter filter)
        {
            ResultObj results;
            try
            {
                var data = Repository.Query(filter).OrderByDescending(x => x.CreatedAt).ToList();
                var total = data.Count();
                if (filter.Pager.Size > 0) data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                var res = data.Select(x =>
                    new
                    {
                        x.Id,
                        x.BatchName,
                        x.Reference,
                        x.Sender,
                        x.Message,
                        x.Title,
                        x.SendAt,
                        x.SendTime,
                        Type = x.Type.ToString(),
                        Status = x.Status.ToString(),
                        RecipientType = x.RecipientType.ToString(),
                        GroupIds = x.Groups?.Split(',').Count(),
                        x.Groups,
                        Recipients = x.Recipients.Count(),
                        x.CreatedAt,
                        x.ModifiedAt,
                        x.CreatedBy,
                        x.ModifiedBy
                    }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().OrderByDescending(x => x.CreatedAt).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.BatchName,
                          x.Reference,
                          x.Sender,
                          x.Message,
                          x.Title,
                          x.SendAt,
                          x.SendTime,
                          Type = x.Type.ToString(),
                          Status = x.Status.ToString(),
                          RecipientType = x.RecipientType.ToString(),
                          GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                          x.Groups,
                          Recipients = x.Recipients.Count(),
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get(new TemplatingsFilter { Id = id }).OrderByDescending(x => x.CreatedAt).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.BatchName,
                          x.Reference,
                          x.Sender,
                          x.Message,
                          x.Title,
                          x.SendAt,
                          x.SendTime,
                          Type = x.Type.ToString(),
                          Status = x.Status.ToString(),
                          RecipientType = x.RecipientType.ToString(),
                          GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                          x.Groups,
                          Recipients = x.Recipients.ToList(),
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).First();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
