﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/messages")]
    public class MessagesController : BaseApi<Message>
    {
        //public override ResultObj Post(Message record)
        //{
        //    using (var db = new AppDbContext())
        //    {
        //        using (var transaction = db.Database.BeginTransaction())
        //        {
        //            ResultObj results;
        //            try
        //            {
        //                var user = User.Identity.AsAppUser().Result;
        //                record.CreatedAt = DateTime.Now;
        //                record.Occupation = null;
        //                record.ModifiedAt = DateTime.Now;
        //                record.CreatedBy = user.UserName;
        //                record.ModifiedBy = user.UserName;
        //                record.Status = PatientStatus.Active;
        //                record.Reference = WebHelpers.GenerateRandomString(32).ToUpper();
        //                if (string.IsNullOrEmpty(record.CardNumber) || string.IsNullOrWhiteSpace(record.CardNumber) || record.CardNumber == "")
        //                    record.CardNumber = WebHelpers.GenerateCardNumber(db);
        //                db.Patients.Add(record);
        //                db.SaveChanges();
        //                var repo = new PatientRepository();
        //                //Save or update id cards
        //                repo.CreateOrUpdateIdentifications(db, idcards, record);

        //                //save or update insurance schemes
        //                repo.CreateOrUpdateInsuranceSchemes(db, insuranceSchemes, record);
        //                transaction.Commit();
        //                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                results = WebHelpers.ProcessException(ex);
        //            }

        //            return results;
        //        }
        //    }
        //}

        public override ResultObj Put(Message record)
        {
            ResultObj results;
            try
            {

                results = WebHelpers.BuildResponse(record, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(MessagesFilter filter)
        {
            ResultObj results;
            try
            {
                var data = Repository.Query(filter).OrderByDescending(x => x.CreatedAt).ToList();
                var total = data.Count();
                if (filter.Pager.Size > 0) data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                var res = data.Select(x =>
                    new
                    {
                        x.Id,
                        x.BatchName,
                        x.Reference,
                        x.Sender,
                        x.Content,
                        x.SendOn,
                        Status = x.Status.ToString(),
                        Type = x.Type.ToString(),
                        x.Receipient,
                        //x.Response,
                        x.Subject,
                        //x.Attachment,
                        x.CreatedAt,
                        x.ModifiedAt,
                        x.CreatedBy,
                        x.ModifiedBy,
                        //x.FileName,
                        //x.FileBase64String,
                        //x.FileExtension
                    }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().OrderBy(x => x.CreatedAt).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.BatchName,
                          x.Reference,
                          x.Sender,
                          x.Content,
                          x.SendOn,
                          Status = x.Status.ToString(),
                          Type = x.Type.ToString(),
                          x.Receipient,
                          //x.Response,
                          x.Subject,
                          //x.Attachment,
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy,
                          //x.FileName,
                          //x.FileBase64String,
                          //x.FileExtension
                      }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get(new MessagesFilter { Id = id }).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.BatchName,
                          x.Reference,
                          x.Sender,
                          x.Content,
                          x.SendOn,
                          Status = x.Status.ToString(),
                          Type = x.Type.ToString(),
                          x.Receipient,
                          x.Response,
                          x.Subject,
                          x.Attachment,
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy,
                          x.FileName,
                          x.FileBase64String,
                          x.FileExtension
                      }).First();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("cancelBatch")]
        public ResultObj CancelBatch(string batchName)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(batchName, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("sendSimple")]
        public ResultObj SendSimple(SimpleMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var batch = record.Type.ToString() + "-" + StringHelpers.GenerateRandomNumber(8);
                        var recp = record.Recipients.Split(',');
                        foreach(var r in recp)
                        {
                            //validate r based on message type
                            var msg = new Message
                            {
                                Type = record.Type,
                                BatchName = batch,
                                Sender = record.Sender,
                                Content = record.Message,
                                SendOn = DateTime.Now,
                                Status = MessageStatus.Pending,
                                Receipient = r,
                                Subject = record.Type.ToString() + " - " + (record.Title.Length <= 20 ? record.Title : record.Title.Substring(0, 20)),
                                CreatedAt = DateTime.Now,
                                CreatedBy = user.UserName,
                                ModifiedAt = DateTime.Now,
                                ModifiedBy = user.UserName,
                            };
                            db.Messages.Add(msg);
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(null, "Initiated.", true, recp.Count());
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

        [HttpPost]
        [Route("sendbulk")]
        public ResultObj SendBulk(BulkMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var batch = record.Type.ToString() + "-" + StringHelpers.GenerateRandomNumber(8);
                        var sendTime = record.SendTime.Split(':').ToList();
                        record.SendAt = new DateTime(record.SendAt.Year, record.SendAt.Month, record.SendAt.Day, int.Parse(sendTime[0]), int.Parse(sendTime[1]), 0);
                        foreach (var r in record.Contacts)
                        {
                            var recp = record.Type == MessageType.SMS ? r.PhoneNumber : r.Email;
                            var message = MessageHelpers.FixContactMessage(record.Message, r);
                            if (string.IsNullOrEmpty(recp)) continue;
                            var msg = new Message
                            {
                                Type = record.Type,
                                BatchName = batch,
                                Sender = record.Sender,
                                Content = message,
                                SendOn = record.SendAt,
                                Status = MessageStatus.Pending,
                                Receipient = recp,
                                Subject = record.Type.ToString() + " - " + (record.Title.Length <= 20 ? record.Title : record.Title.Substring(0, 20)),
                                CreatedAt = DateTime.Now,
                                CreatedBy = user.UserName,
                                ModifiedAt = DateTime.Now,
                                ModifiedBy = user.UserName,
                            };
                            db.Messages.Add(msg);
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(null, "Initiated.", true, record.Contacts.Count());
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }
    }
}
