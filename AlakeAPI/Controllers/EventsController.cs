﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.DataAccess.Repositories;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/events")]
    public class EventsController : BaseApi<Event>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var data =
                    Repository.Get()
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Title,
                                    x.Code,
                                    x.Location,
                                    x.Description,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive
                                })
                        .ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("getactive")]
        public ResultObj GetActive()
        {
            ResultObj results;
            try
            {
                //var user = User.Identity.AsAppUser().Result;
                var data =
                    Repository.Get()
                        .Where(x => x.IsActive)
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    x.Title,
                                    x.Code,
                                    x.Location,
                                    x.Description,
                                    x.StartDate,
                                    x.EndDate,
                                    x.Logo,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy,
                                    x.IsActive
                                })
                        .FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Record Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Post(Event record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.IsActive = false;
                record.Code = StringHelpers.GenerateRandomNumber(15);
                Repository.Insert(SetAudit(record, true));
                new EventRepository().UpdateEventCode(record.Id);
                results = WebHelpers.BuildResponse(record, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("saveattendant")]
        public ResultObj SaveAttendant(EventAttendant record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var activeEvent = new EventRepository().GetActiveEvent();
                record.EventId = activeEvent.Id;
                record.CreatedAt = DateTime.Now;
                record.CreatedBy = "System";
                record.ModifiedAt = DateTime.Now;
                record.ModifiedBy = "System";
                record.MessageSent = true;
                db.EventAttendants.Add(record);

                //sendmessage
                var message = $"Dear {record.FullName}, welcome to the {activeEvent.Title}. Your registration code is {record.Code}. Thank you.";
                var msg = new Message
                {
                    Type = MessageType.SMS,
                    BatchName = StringHelpers.GenerateRandomNumber(6),
                    Sender = SetupConfig.Setting.SmsMessageService.SenderId,
                    Content = message,
                    SendOn = DateTime.Now,
                    Status = MessageStatus.Pending,
                    Receipient = record.PhoneNumber,
                    Subject = "Event Registration - "+activeEvent.Code,
                    CreatedAt = DateTime.Now,
                    CreatedBy = "System",
                    ModifiedAt = DateTime.Now,
                    ModifiedBy = "System",
                };
                db.Messages.Add(msg);

                db.SaveChanges();
                results = WebHelpers.BuildResponse(record, "Registration Successful", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        public override ResultObj Put(Event record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                Repository.Update(SetAudit(record, true));
                new EventRepository().UpdateEventCode(record.Id);
                results = WebHelpers.BuildResponse(record, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }
        [HttpGet]
        [Route("activate")]
        public ResultObj Activate(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                new EventRepository().Activate(id);
                results = WebHelpers.BuildResponse(null, "Record has been activated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("deactivate")]
        public ResultObj Deactivate(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                new EventRepository().Deactivate(id);
                results = WebHelpers.BuildResponse(null, "Record has been deactivated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(EventsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var data = filter.BuildQuery(new AppDbContext().Events).ToList().OrderBy(x => x.StartDate).ToList();
                var total = data.Count();
                if (filter.Pager.Page > 0)
                {
                    data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                }
                var res = data.Select(x => new
                {
                    x.Id,
                    x.Title,
                    x.Code,
                    x.Location,
                    x.Description,
                    x.StartDate,
                    x.EndDate,
                    x.Logo,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy,
                    x.IsActive
                }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getattendants")]
        public ResultObj GetAttendants()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var eventRepo = new EventRepository();
                var activeEvent = eventRepo.ActiveEvent(db);
                
                var data = db.EventAttendants.Where(x => x.EventId == activeEvent.Id).Select(
                    x =>
                        new
                        {
                            x.Id,
                            Event = x.Event.Title,
                            x.FullName,
                            x.PhoneNumber,
                            x.Email,
                            x.Company,
                            x.Position,
                            x.MessageSent,
                            x.TagPrinted,
                            x.CreatedAt,
                            x.ModifiedAt,
                            x.CreatedBy,
                            x.ModifiedBy
                        })
                    .ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getallattendants")]
        public ResultObj GetAllAttendants()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;

                var data = db.EventAttendants.Where(x => x.Id > 0).Select(
                    x =>
                        new
                        {
                            x.Id,
                            Event = x.Event.Title,
                            x.FullName,
                            x.PhoneNumber,
                            x.Email,
                            x.Company,
                            x.Position,
                            x.MessageSent,
                            x.TagPrinted,
                            x.CreatedAt,
                            x.ModifiedAt,
                            x.CreatedBy,
                            x.ModifiedBy
                        })
                    .ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getattendant")]
        public ResultObj GetAttendant(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var data = db.EventAttendants.Where(x => x.Id == id).Select(
                    x =>
                        new
                        {
                            x.Id,
                            Event = x.Event.Title,
                            x.FullName,
                            x.PhoneNumber,
                            x.Email,
                            x.Company,
                            x.Position,
                            x.MessageSent,
                            x.TagPrinted,
                            x.CreatedAt,
                            x.ModifiedAt,
                            x.CreatedBy,
                            x.ModifiedBy
                        })
                    .FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getbyevent")]
        public ResultObj GetbyEvent(long eventId)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var data =
                    db.EventAttendants
                        .Where(x => x.EventId == eventId)
                        .ToList()
                        .Select(
                            x =>
                                new
                                {
                                    x.Id,
                                    Event = x.Event.Title,
                                    x.FullName,
                                    x.PhoneNumber,
                                    x.Email,
                                    x.Company,
                                    x.Position,
                                    x.MessageSent,
                                    x.TagPrinted,
                                    x.CreatedAt,
                                    x.ModifiedAt,
                                    x.CreatedBy,
                                    x.ModifiedBy
                                })
                        .ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("queryattendants")]
        public ResultObj QueryAttendants(EventAttendantsFilter filter)
        {
            ResultObj results;
            try
            {
                var eventRepo = new EventRepository();
                var user = User.Identity.AsAppUser().Result;
                //var activeEvent = eventRepo.ActiveEvent(new AppDbContext());
                var data = filter.BuildQuery(new AppDbContext().EventAttendants).ToList().OrderBy(x => x.FullName).ToList();
                var total = data.Count();
                if (filter.Pager.Page > 0)
                {
                    data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                }
                var res = data.Select(x => new
                {
                    x.Id,
                    Event = x.Event.Title,
                    x.FullName,
                    x.PhoneNumber,
                    x.Email,
                    x.Company,
                    x.Position,
                    x.MessageSent,
                    x.TagPrinted,
                    x.CreatedAt,
                    x.ModifiedAt,
                    x.CreatedBy,
                    x.ModifiedBy
                }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
