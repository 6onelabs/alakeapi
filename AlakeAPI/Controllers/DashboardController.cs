﻿using AlakeAPI.AxHelpers;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        [HttpGet]
        [Route("stats")]
        public ResultObj Stats()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var stats = new DashboardStats();
                //stats.Groups = db.Groups.Where(x => !x.IsDeleted).Count();
                //stats.Senders = db.Senders.Where(x => !x.IsDeleted).Count();
                var conts = db.Contacts.Where(x => x.Id > 0);
                stats.Contacts = conts.Count();
                stats.ActiveContacts = conts.Where(x => x.Status == ContactStatus.Active).Count();
                stats.BlockedContacts = conts.Where(x => x.Status == ContactStatus.Blocked).Count();

                var msgs = db.Messages.Where(x => x.Id > 0);
                stats.TotalMessages = msgs.Count();
                stats.FailedSms = msgs.Where(x => x.Type == MessageType.SMS && x.Status == MessageStatus.Failed).Count();
                stats.FailedEmails = msgs.Where(x => x.Type == MessageType.Email && x.Status == MessageStatus.Failed).Count();
                stats.PendingSms = msgs.Where(x => x.Type == MessageType.SMS && x.Status == MessageStatus.Pending).Count();
                stats.PendingEmails = msgs.Where(x => x.Type == MessageType.Email && x.Status == MessageStatus.Pending).Count();
                stats.SentSms = msgs.Where(x => x.Type == MessageType.SMS && x.Status == MessageStatus.Sent).Count();
                stats.SentEmails = msgs.Where(x => x.Type == MessageType.Email && x.Status == MessageStatus.Sent).Count();

                stats.PercentageFailed = ((stats.FailedEmails + stats.FailedSms) / stats.TotalMessages) * 100;
                stats.PercentagePending = ((stats.PendingEmails + stats.PendingSms) / stats.TotalMessages) * 100;
                stats.PercentageSent = ((stats.SentEmails + stats.SentSms) / stats.TotalMessages) * 100;


                results = WebHelpers.BuildResponse(stats, "Successful", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("balance")]
        public ResultObj Balance()
        {
            ResultObj results;
            try
            {
                var bal = 0.0;
                var user = User.Identity.AsAppUser().Result;

                var client = new RestClient(SetupConfig.Setting.SmsMessageService.BalanceUrl);
                var req = new RestRequest(Method.GET)
                {
                    RequestFormat = DataFormat.Json
                };
                req.AddHeader("Accept", "application/json");
                req.AddHeader("Content-Type", "application/json");
                req.AddHeader("Authorization", SetupConfig.Setting.SmsMessageService.BasicToken);

                var res = client.Execute(req);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    var result = JsonConvert.DeserializeObject<DashboardBalance>(res.Content);
                    bal = result.balance;
                }
                    results = WebHelpers.BuildResponse(bal, "Successful", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("charts")]
        public ResultObj Charts()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var now = DateTime.Now;
                var lb1 = new List<string> { "Jan", "Feb", "Mar", "Apr", "May", "June"};
                var p1 = "January - June";
                var lb2 = new List<string> { "July", "Aug", "Sept", "Oct", "Nov", "Dec" };
                var p2 = "July - December";
                var chart = new ChartData();
                var msgs = db.Messages.Where(x => x.CreatedAt.Year == now.Year);
                chart.Series = new List<string> { "SMS", "Email" };
                chart.Labels = lb1;
                chart.Period = p1;
                if (now.Month > 6)
                {
                    chart.Labels = lb2;
                    chart.Period = p2;
                }
                var dt = new List<List<int>>();
                foreach(var s in chart.Series)
                {
                    var sMessages = msgs.Where(x => x.Type.ToString() == s).ToList();
                    var d = new List<int>();
                    foreach (var l in chart.Labels)
                    {
                        var cl = sMessages.Where(x => x.CreatedAt.ToString("MMM", CultureInfo.CreateSpecificCulture("en-US")) == l).Count();
                        d.Add(cl);
                    }
                    dt.Add(d);
                }
                chart.Data = dt;               

                results = WebHelpers.BuildResponse(chart, "Successful", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("birthdays")]
        public ResultObj Birthdays()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var now = DateTime.Now;
                var res = db.Contacts.Where(x => x.DateOfBirth.HasValue && x.DateOfBirth.Value.Month >= now.Month && x.DateOfBirth.Value.Day >= now.Day).ToList().Select(x => new { x.Id, x.Name, DateOfBirth = new DateTime(now.Year, x.DateOfBirth.Value.Month, x.DateOfBirth.Value.Day) }).OrderBy(x => x.DateOfBirth).Take(7).ToList();

                results = WebHelpers.BuildResponse(res, "Successful", true, res.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
    public class DashboardStats
    {
        public int Senders { get; set; }
        public int Groups { get; set; }
        public int Contacts { get; set; }
        public int ActiveContacts { get; set; }
        public int BlockedContacts { get; set; }
        public int TotalMessages { get; set; }
        public double PercentageFailed { get; set; }
        public double PercentagePending { get; set; }
        public double PercentageSent { get; set; }
        public int FailedSms { get; set; }
        public int FailedEmails { get; set; }
        public int SentSms { get; set; }
        public int SentEmails { get; set; }
        public int PendingSms { get; set; }
        public int PendingEmails { get; set; }
    }

    public class ChartData
    {
        public List<string> Labels { get; set; }
        public List<string> Series { get; set; }
        public List<List<int>> Data { get; set; }
        public string Period { get; set; }
    }

    public class DashboardBalance
    {
        public double balance { get; set; }
        public string currency { get; set; }
    }
}
