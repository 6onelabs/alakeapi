﻿using AlakeAPI.AxHelpers;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/profile")]
    public class ProfileController : BaseApi<Profile>
    {
        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var data = Repository.Get().OrderBy(x => x.Name).Select(x =>
                     new
                     {
                         x.Id,
                         x.Name,
                         x.Privileges,
                         x.Notes,
                         x.Locked,
                         x.ModifiedAt,
                         x.ModifiedBy,
                         x.CreatedAt,
                         x.CreatedBy
                     }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Post(Profile record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.ModifiedAt = DateTime.Now;
                record.ModifiedBy = user.UserName;
                record.CreatedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                Repository.Insert(record);
                results = WebHelpers.BuildResponse(record, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        public override ResultObj Put(Profile record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.ModifiedAt = DateTime.Now;
                record.ModifiedBy = user.UserName;
                
                Repository.Update(SetAudit(record, true));
                results = WebHelpers.BuildResponse(record, "Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

    }
}
