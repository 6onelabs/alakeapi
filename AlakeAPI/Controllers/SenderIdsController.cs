﻿using AlakeAPI.Models;

namespace AlakeAPI.Controllers
{
    public class SenderIdsController : BaseApi<Sender>
    { }

    public class TemplatesController : BaseApi<Template>
    { }
}
