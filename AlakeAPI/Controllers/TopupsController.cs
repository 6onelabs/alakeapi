﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/topups")]
    public class TopupsController : BaseApi<TopUp>
    {
        public override ResultObj Post(TopUp record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        record.CreatedAt = DateTime.Now;
                        record.ModifiedAt = DateTime.Now;
                        record.CreatedBy = user.UserName;
                        record.ModifiedBy = user.UserName;
                        db.TopUps.Add(record);
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        public override ResultObj Put(TopUp record)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(null, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(TopupsFilter filter)
        {
            ResultObj results;
            try
            {
                var data = Repository.Query(filter).OrderByDescending(x => x.CreatedAt).ThenBy(x => x.Status).ToList();
                var total = data.Count();
                if (filter.Pager.Size > 0) data = data.Skip(filter.Pager.Skip()).Take(filter.Pager.Size).ToList();
                var res = data.Select(x =>
                    new
                    {
                        x.Id,
                        x.Amount,
                        x.Tax,
                        TotalPaid = x.Amount +x.Tax,
                        x.PhoneNumber,
                        x.Network,
                        x.Token,
                        Status = x.Status.ToString(),
                        x.CreatedAt,
                        x.ModifiedAt,
                        x.CreatedBy,
                        x.ModifiedBy
                    }).ToList();

                results = WebHelpers.BuildResponse(res, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().OrderByDescending(x => x.CreatedAt).ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Amount,
                           x.Tax,
                           TotalPaid = x.Amount + x.Tax,
                           x.PhoneNumber,
                           x.Network,
                           x.Token,
                           Status = x.Status.ToString(),
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get(new TopupsFilter { Id = id }).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.Amount,
                          x.Tax,
                          TotalPaid = x.Amount + x.Tax,
                          x.PhoneNumber,
                          x.Network,
                          x.Token,
                          Status = x.Status.ToString(),
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).First();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(null, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("cancel")]
        public ResultObj Cancel(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.TopUps.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Could not find the topup");
                if (rec.Status == TopupStatus.Cancelled) throw new Exception("Topup is already cancelled");
                rec.Status = TopupStatus.Cancelled;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Cancelled", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("processstatus")]
        public ResultObj ProcessStatus(object obj)
        {
            ResultObj results;
            try
            {
                

                results = WebHelpers.BuildResponse(obj, "Successful", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
