﻿using AlakeAPI.AxHelpers;
using AlakeAPI.DataAccess.Filters;
using AlakeAPI.Extensions;
using AlakeAPI.Models;
using System;
using System.Linq;
using System.Web.Http;

namespace AlakeAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/schedulemessages")]
    public class SheduleMessagesController : BaseApi<ScheduleMessage>
    {
        public override ResultObj Put(ScheduleMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleMessages.Where(x => x.Id == record.Id && x.Type == record.Type).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        //msg.Title = record.Title;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = string.Join(",", record.GroupIds);
                        msg.SendOn = record.SendOn;
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

        public override ResultObj Post(ScheduleMessage record)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(record, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(null, "Not Implemented", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().Where(x=> x.Id == id).ToList().Select(x =>
                      new
                      {
                          x.Id,
                          x.Reference,
                          x.Sender,
                          x.Message,
                          x.SendOn,
                          x.Groups,
                          MessageType = x.MessageType.ToString(),
                          Status = x.Status.ToString(),
                          Type = x.Type.ToString(),
                          RecipientType = x.RecipientType.ToString(),
                          x.Title,
                          GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                          x.ContactIds,
                          x.CreatedAt,
                          x.ModifiedAt,
                          x.CreatedBy,
                          x.ModifiedBy
                      }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getbytype")]
        public ResultObj GetByType(ScheduleMessageType type)
        {
            ResultObj results;
            try
            {
                var data = Repository.Get().Where(x => x.Type == type).ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Reference,
                           x.Sender,
                           x.Message,
                           x.SendOn,
                           x.Groups,
                           MessageType = x.MessageType.ToString(),
                           Status = x.Status.ToString(),
                           Type = x.Type.ToString(),
                           RecipientType = x.RecipientType.ToString(),
                           x.Title,
                           GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                           x.ContactIds,
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpPost]
        [Route("update")]
        public ResultObj UpdateSchedule(ScheduleMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleMessages.Where(x => x.Id == record.Id && x.Type == record.Type).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        msg.Status = record.Status;
                        //msg.Title = record.Title;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = record.GroupIds == null ? null : string.Join(",", record.GroupIds);
                        msg.SendOn = record.SendOn;
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }


        [HttpGet]
        [Route("getbirthday")]
        public ResultObj GetBirthday()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ScheduleBirthdayMessages.Where(x => x.Title == "Happy Birthday").ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Reference,
                           x.Sender,
                           x.Message,
                           x.Groups,
                           MessageType = x.MessageType.ToString(),
                           Status = x.Status.ToString(),
                           RecipientType = x.RecipientType.ToString(),
                           x.Title,
                           GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                           x.ContactIds,
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpPost]
        [Route("updatebirthday")]
        public ResultObj UpdateBirthday(ScheduleBirthdayMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleBirthdayMessages.Where(x => x.Id == record.Id).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        msg.Status = record.Status;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = record.GroupIds == null ? null : string.Join(",", record.GroupIds);
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

        [HttpGet]
        [Route("getretirement")]
        public ResultObj GetRetirement()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ScheduleRetirementMessages.Where(x => x.Title == "Retirement Notice").ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Reference,
                           x.Sender,
                           x.Message,
                           x.Groups,
                           MessageType = x.MessageType.ToString(),
                           Status = x.Status.ToString(),
                           RecipientType = x.RecipientType.ToString(),
                           x.Title,
                           GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                           x.ContactIds,
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpPost]
        [Route("updateretirement")]
        public ResultObj UpdateRetirement(ScheduleRetirementMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleRetirementMessages.Where(x => x.Id == record.Id).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        msg.Status = record.Status;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = record.GroupIds == null ? null : string.Join(",", record.GroupIds);
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

        [HttpGet]
        [Route("getpreretirement")]
        public ResultObj GetPreRetirement()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ScheduleRetirementMessages.Where(x => x.Title == "Retirement 6 Months Notice").ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Reference,
                           x.Sender,
                           x.Message,
                           x.Groups,
                           MessageType = x.MessageType.ToString(),
                           Status = x.Status.ToString(),
                           RecipientType = x.RecipientType.ToString(),
                           x.Title,
                           GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                           x.ContactIds,
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpPost]
        [Route("updatepreretirement")]
        public ResultObj UpdatePreRetirement(ScheduleRetirementMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleRetirementMessages.Where(x => x.Id == record.Id).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        msg.Status = record.Status;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = record.GroupIds == null ? null : string.Join(",", record.GroupIds);
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

        [HttpGet]
        [Route("getworkanniversary")]
        public ResultObj GetWorkAnniversary()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ScheduleRetirementMessages.Where(x => x.Title == "Work Anniversary").ToList().Select(x =>
                       new
                       {
                           x.Id,
                           x.Reference,
                           x.Sender,
                           x.Message,
                           x.Groups,
                           MessageType = x.MessageType.ToString(),
                           Status = x.Status.ToString(),
                           RecipientType = x.RecipientType.ToString(),
                           x.Title,
                           GroupIds = x.Groups?.Split(',').Select(g => int.Parse(g)).ToList(),
                           x.ContactIds,
                           x.CreatedAt,
                           x.ModifiedAt,
                           x.CreatedBy,
                           x.ModifiedBy
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpPost]
        [Route("updateworkanniversary")]
        public ResultObj UpdateWorkAnniversary(ScheduleRetirementMessage record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var msg = db.ScheduleRetirementMessages.Where(x => x.Id == record.Id).FirstOrDefault();
                        if (msg == null) throw new Exception("Action not possible");

                        msg.ModifiedAt = DateTime.Now;
                        msg.ModifiedBy = user.UserName;
                        msg.Sender = record.Sender;
                        msg.Message = record.Message;
                        msg.Status = record.Status;
                        msg.RecipientType = record.RecipientType;
                        msg.ContactIds = record.ContactIds;
                        msg.Groups = record.GroupIds == null ? null : string.Join(",", record.GroupIds);
                        msg.MessageType = record.MessageType;
                        msg.Title = record.Title;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }
                    return results;
                }
            }
        }

    }
}
